/**
 * @format
 */

import * as React from 'react';
import {AppRegistry} from 'react-native'; // JS entry point to running all React Native
import {Provider as PaperProvider} from 'react-native-paper'; // 3rd party UI library
import App from './src/App'; // component
import {name as appName} from './app.json';

export default function Main() {
  return (
    <PaperProvider>
      <App></App>
    </PaperProvider>
  );
}

// register app name and main component
AppRegistry.registerComponent(appName, () => Main);
