import 'react-native-gesture-handler'; // put this at the very first

import React, {useState, useEffect} from 'react';

/**
 * managing app state and linking top-level navigator to the app environment
 * more info: https://reactnavigation.org/docs/navigation-container/
 */
import {NavigationContainer} from '@react-navigation/native';

/**
 * provides a way for your app to transition between screens where each new screen is placed on top of a stack.
 * more info: https://reactnavigation.org/docs/stack-navigator/
 */
import {createStackNavigator} from '@react-navigation/stack';

// components of each page
import Home from '../components/home/homePage';
import IntroPage from '../components/intro/introPage';
import PicPage from '../components/vlpic/picPage';
import GifPage from '../components/vlgif/gifPage';
import GamePage from '../components/vlgame/gamePage';
import CloudPage from '../components/cloud/cloudPage';

// create stack instance
const Stack = createStackNavigator();

/*
navigation stack
define navigation tag(name) and it's according destination(component)
*/
MainNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomePage">
        <Stack.Screen name="IntroPage" component={IntroPage} />
        <Stack.Screen name="HomePage" component={Home} />
        <Stack.Screen name="PicPage" component={PicPage} />
        <Stack.Screen name="GifPage" component={GifPage} />
        <Stack.Screen name="GamePage" component={GamePage} />
        <Stack.Screen name="CloudPage" component={CloudPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

// export the module so you can import later
export default MainNavigation;
