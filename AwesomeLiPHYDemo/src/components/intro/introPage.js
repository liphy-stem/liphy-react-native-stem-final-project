import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, SafeAreaView} from 'react-native'; // import basic react native components

import LightManager from 'react-native-liphy'; // import liphy sdk

export default IntroPage = () => {
  const [lightId, setLightId] = useState(); // state of light id & setter

  useEffect(() => {
    // setup lightManager to capture liphy light
    LightManager.startTracking();
    LightManager.setIsFront(false);

    const listener = LightManager.addEventListener(data => {
      console.log('light id in introPage: ' + data.lightId);
      setLightId(data.lightId ? data.lightId : 'undefined light ID');
    });

    return () => {
      console.log('return in useEffect() in introPage');
      listener();
    };
  }, [lightId]); // lightId is the dependency here: when lightId changes the effect hook will be triggered

  return (
    <SafeAreaView style={styles.container}>
      <Text
        // applying style defined in styles variable (at the end of the file)
        style={{
          ...styles.message,
        }}>
        Scan a light to get the light ID!
      </Text>

      <View style={{display: 'flex'}}>
        {/* when light is 53 change the background to yellow & text to 53*/}
        {lightId === '53' ? (
          <Text
            style={{
              // custom styles other than styles.lightIdText
              backgroundColor: '#1cb1dc', // background color, blue
              color: '#efefef', // font color, grey
              ...styles.lightIdText, // lightIdText style defined in styles variable
            }}>
            53
          </Text>
        ) : null}

        {/* when light is 54 change the background to blue & text to 54 */}
        {lightId === '54' ? (
          <Text
            style={{
              // custom styles other than styles.lightIdText
              backgroundColor: '#e1c139', // background color, yellow
              color: '#000', // font color, black
              ...styles.lightIdText, // lightIdText style defined in styles variable
            }}>
            54
          </Text>
        ) : null}

        {/* when light is neither 53/54 then change the background to grey */}
        {lightId !== '53' && lightId !== '54' ? (
          <Text
            style={{
              ...styles.lightIdText,
              backgroundColor: '#fefefe', // background color, grey
            }}>
            {lightId ? lightId : '_'}
          </Text>
        ) : null}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  message: {
    backgroundColor: '#fefefe',
    color: '#666',
    overflow: 'hidden',
    marginBottom: 20,
    borderRadius: 10,
    width: '95%',
    padding: 30,

    // font
    fontWeight: 'bold',
    fontSize: 20,
  },
  lightIdText: {
    borderRadius: 10,
    overflow: 'hidden',
    width: 300,
    padding: 30,
    marginBottom: 150,

    // font
    fontSize: 20,
    textAlign: 'center',
  },
});
