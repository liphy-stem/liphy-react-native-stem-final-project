import React, {useState, useEffect} from 'react';
import {Text, StyleSheet, View, Image} from 'react-native'; // import basic react native components

import LightManager from 'react-native-liphy'; // import liphy sdk
import {getImageUrl as getImageFromWeb} from '../../utils/utils'; // import helper function

// single page application with gifs reactive to liphy lights

const PicPage = () => {
  const width = 300;
  const height = 300;
  const message = 'Now scan a LiPHY light to get a light ID and a photo!!';

  // liphy
  const [lightId, setLightId] = useState();

  useEffect(() => {
    // setup lightManager
    LightManager.startTracking();
    LightManager.setIsFront(false);

    const listener = LightManager.addEventListener(data => {
      console.log('light id in picPage: ' + data.lightId);
      setLightId(data.lightId ? data.lightId : 'undefined light ID');
    });

    return () => {
      console.log('return in useEffect() in PicPage');
      listener();
    };
  }, [lightId]);

  if (lightId === undefined || lightId === '') {
    // when no light id is received
    return (
      <View style={styles.container}>
        <View style={{flex: 1}} />
        <Text style={styles.lightIdBox}>{message}</Text>

        {/* empty image placeholder */}
        <View
          style={{
            ...styles.image,
            width: width,
            height: height,
          }}
        />

        <View style={{flex: 1}} />
      </View>
    );
  } else {
    // valid light ids

    // check url
    // console.log(getImageUrl(lightId, 200, 200));

    return (
      <View style={styles.container}>
        <Text style={styles.lightIdBox}>Light ID: {lightId}</Text>
        <Image
          style={{...styles.image, width: width, height: height}}
          source={{
            uri: getImageFromWeb(lightId, width, height),
          }}
        />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lightIdBox: {
    borderRadius: 20,
    overflow: 'hidden',
    padding: 30,
    marginBottom: 20,
    fontSize: 20,
    color: '#333',
    backgroundColor: '#fefefe',
  },
  image: {
    borderRadius: 20,
    overflow: 'hidden',
    backgroundColor: '#444444',
    marginBottom: 100,
  },
});

export default PicPage;
