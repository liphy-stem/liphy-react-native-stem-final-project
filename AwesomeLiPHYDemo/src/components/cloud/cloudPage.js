import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native'; // import basic react native components

const CloudPage = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 24, fontWeight: 'bold'}}>Coming soon!!</Text>
      <Text style={{fontSize: 20, fontWeight: 'normal', padding: 25}}>
        Interact with our cloud server and learn how cloud works
      </Text>

      {/* <Text>cloud page</Text>
      <Text>setup cloud</Text>
      <Text>scan a light to get content</Text>
      <Text>modify the content</Text>
      <Text>scan the light again</Text> */}
    </View>
  );
};

export default CloudPage;
