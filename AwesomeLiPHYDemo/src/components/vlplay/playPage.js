import React, {useState, useEffect, useCallback} from 'react';

import {View, Text, StyleSheet, SafeAreaView, Image} from 'react-native'; // import basic react native components
import Sound from 'react-native-sound';
import Video from 'react-native-video';

import {Button} from 'react-native-paper';

import moduleName from '../../../test.mp3';
import useWindowDimensions from 'react-native/Libraries/Utilities/useWindowDimensions';

import LightManager from 'react-native-liphy'; // import liphy sdk

const PlayPage = () => {
  Sound.setCategory('Playback');

  const screenWidth = useWindowDimensions().width;

  const [buttonTexts, setButtonTexts] = useState([
    {name: 'GIF', tag: 1},
    // {name: 'Video', tag: 2},
    // {name: 'Song', tag: 3},
  ]);
  const [activeTag, setActiveTag] = useState(1);
  const [song, setSong] = useState(
    new Sound('chillSong.mp3', Sound.MAIN_BUNDLE),
  );

  const [lightId, setLightId] = useState();
  const [lastScanTime, setLastScanTime] = useState();
  const [lastTriggerTime, setLastTriggerTime] = useState();
  const [stop, setStop] = useState(true);
  var timer;

  useEffect(() => {
    // setup lightManager
    LightManager.startTracking();
    LightManager.setIsFront(false);

    const listener = LightManager.addEventListener(data => {
      setLightId(data.lightId ? data.lightId : 'undefined light ID');

      // play media
      setStop(false);

      var currentdate = new Date();
      var datetime =
        'time: ' +
        currentdate.getHours() +
        ':' +
        currentdate.getMinutes() +
        ':' +
        currentdate.getSeconds() +
        ':' +
        currentdate.getMilliseconds();

      setLastScanTime(currentdate);

      console.log(`light id: ${data.lightId}, ${datetime}`);

      console.log(activeTag);
    });

    return () => {
      listener();
    };
  }, [lightId]);

  // useEffect(() => {
  //   return () => {
  //     if (lastScanTime !== undefined) {
  //       console.log(
  //         `last scan time: ${lastScanTime.getMinutes()}:${lastScanTime.getSeconds()}:${lastScanTime.getMilliseconds()}`,
  //       );

  //       setStop(false);

  //       if (lastTriggerTime !== undefined) {
  //         const diff = (lastScanTime - lastTriggerTime) / 1000;

  //         // if last trigger time is >1s after last timer setup -> reset timer
  //         if (diff > 1000) {
  //           clearTimeout(timer);

  //           timer = setTimeout(() => {
  //             setStop(true);
  //           }, 2000);

  //           setLastTriggerTime(new Date());
  //         }
  //       } else {
  //         // first time
  //         timer = setTimeout(() => {
  //           // var currentTime = new Date();
  //           // // if current time > last scan time for more than 1s => stop playing
  //           // const diff = (currentTime - lastScanTime) / 1000;
  //           // console.log(`diff: ${diff}`);
  //           // cancel the media
  //           setStop(true);
  //         }, 2000);

  //         setLastTriggerTime(new Date());
  //       }
  //     }
  //   };
  // }, [lastScanTime]);

  useEffect(() => {
    const checkTime = setTimeout(() => {
      setStop(true);
    }, 2000);

    return () => {
      if (checkTime !== undefined) {
        clearTimeout(checkTime);
      }

      if (lastScanTime !== false) {
        checkTime;
      }
    };
  }, [lastScanTime]);

  // useEffect(() => {
  //   return () => {
  //     console.log('current tag: ' + activeTag);

  //     // // cancel song
  //     // if (activeTag !== 3) {
  //     //   console.log('current tag is not 3');
  //     //   console.log('sound: ' + sound.getDuration());

  //     //   sound.pause();
  //     //   sound.stop(() => {
  //     //     console.log('stopped song');
  //     //   });
  //     //   sound.release();
  //     // }
  //   };
  // }, [activeTag, sound]);

  const onButtonPressed = useCallback(
    item => {
      switch (item.tag) {
        case 1:
          // playGif();
          // cancelSong();

          // play gif in mediaContainer
          setActiveTag(1);
          break;
        // case 2:
        //   playVideo();
        //   cancelSong();
        //   break;
        // case 3:
        //   playSong();
        //   break;
        default:
          console.log(
            `something wrong with the buttons in PlayPage. Tag: ${item.tag}`,
          );
      }

      console.log('current tag: ' + activeTag);

      // // cancel video if pressing other buttons
      // if (item.tag !== 2) {
      // }

      // // cancel song if pressing other buttons
      // if (item.tag !== 3) {
      //   console.log('current tag is not 3');
      //   song.getCurrentTime(seconds =>
      //     console.log('song current time at ' + seconds),
      //   );
      // }
    },
    [activeTag, song],
  );

  // const playSongCallback = (error, song) => {
  //   console.log('song loaded');

  //   if (error) {
  //     Alert.alert('error', error.message);
  //     console.log(error.message);
  //     return;
  //   }

  //   song.play(success => {
  //     if (success) {
  //       console.log('successfully finished playing');
  //       song.release();
  //     } else {
  //       console.log('playback failed due to audio decoding errors');
  //     }
  //   });
  // };

  // const cancelSong = () => {
  //   // song.pause(() => {
  //   //   console.log('song paused');
  //   // });
  //   song.stop(() => {
  //     console.log('song stopped');
  //   });
  //   song.release();
  // };

  // const playSong = () => {
  //   console.log('---playing song');
  //   setActiveTag(3);

  //   // play the song

  //   /*

  //   put some more song here for students to try

  //   - www.ultimatetaxi.com/sound/slot.wav
  //   */

  //   // cancel last song if pressed again
  //   // cancelSong();

  //   // getting sounds from internet
  //   // const sound = new Sound('www.ultimatetaxi.com/sound/slot.wav', '', error =>

  //   // getting sounds locally
  //   setSong(
  //     new Sound('chillSong.mp3', Sound.MAIN_BUNDLE, error =>
  //       playSongCallback(error, song),
  //     ),
  //   );
  // };

  // const playGif = () => {
  //   console.log('playing gif');
  // };

  // const playVideo = () => {
  //   console.log('playing video');

  //   // play video in mediaContainer
  //   setActiveTag(2);
  // };

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.header}>Play gif, video & songs with LiPHY</Text>

      <View style={styles.mediaContainer}>
        {activeTag === 1 && stop === false ? (
          <Image
            style={{width: 230, height: 230}}
            source={{
              uri: 'https://media.giphy.com/media/Kx82Lvb7wcYNO/giphy.gif',
            }}
          />
        ) : null}
        {activeTag === 2 && stop === false ? (
          <Video
            source={
              require('../../assets/leaves.mp4')
              // {uri:'https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4?_=1',}
            }
            style={{width: 270, height: 270}}
            controls={true}
            ref={ref => {
              this.player = ref;
            }}
          />
        ) : null}

        {activeTag === 3 && stop === false ? (
          <Image
            style={{width: 230, height: 230}}
            source={
              require('../../assets/images/playButton.jpg')
              // { uri: 'https://source.unsplash.com/niUkImZcSP8/500x500',}
            }
          />
        ) : null}
      </View>

      {/* <FlatList
        style={{width: screenWidth}}
        keyExtractor={(item, index) => index.toString()}
        data={buttonTexts}
        renderItem={({item, index}) => {
          return (
            <Button
              style={{
                width: screenWidth / 4,
                height: 50,
                padding: 6,
                margin: screenWidth / 24, // (1/4) / 6 = 1/24  [1/4 space left, divide by 6 margin in total (2 from each button)]
              }}
              // icon=""
              mode="contained"
              onPress={() => {
                onButtonPressed(item);
              }}>
              {item.name}
            </Button>
          );
        }}
        scrollEnabled={false}
        horizontal={true}
      /> */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingTop: 35,
    padding: 20,
  },
  mediaContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 300,
    width: 300,
    margin: 50,
    backgroundColor: '#bbb',
    borderRadius: 30,
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default PlayPage;
