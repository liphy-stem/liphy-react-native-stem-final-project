import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, SafeAreaView, Image} from 'react-native'; // import basic react native components

import useWindowDimensions from 'react-native/Libraries/Utilities/useWindowDimensions';

import LightManager from 'react-native-liphy'; // import liphy sdk

const GifPage = () => {
  const [lightId, setLightId] = useState();
  const [lastScanTime, setLastScanTime] = useState();
  const [stop, setStop] = useState(true);

  useEffect(() => {
    // setup lightManager
    LightManager.startTracking();
    LightManager.setIsFront(false);

    const listener = LightManager.addEventListener(data => {
      setLightId(data.lightId ? data.lightId : 'undefined light ID');

      // play media
      setStop(false);

      // create Date object
      var currentdate = new Date();

      // record scan time for reference
      setLastScanTime(currentdate);

      //
      var datetime =
        'time: ' +
        currentdate.getHours() +
        ':' +
        currentdate.getMinutes() +
        ':' +
        currentdate.getSeconds() +
        ':' +
        currentdate.getMilliseconds();

      // logging time when setting light id
      console.log(`light id: ${data.lightId}, ${datetime}`);
    });

    return () => {
      listener();
    };
  }, [lightId]); // trigger when lightId changes

  useEffect(() => {
    // function for setting stop as true after 2s
    const checkTime = setTimeout(() => {
      setStop(true);
    }, 2000);

    return () => {
      // this block of code will be triggered when lastScanTime is changed (when scanning a light, lastScanTime will be set)

      // clear last timeout if any
      if (checkTime !== undefined) {
        clearTimeout(checkTime);
      }

      // avoid triggering when lastScanTime is undefined
      if (lastScanTime !== undefined) {
        // start timer after
        checkTime;
      }
    };
  }, [lastScanTime]); // trigger when lastScanTime changes

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.header}>
        Scan a LiPHY light and play the hidden gif!
      </Text>

      <View style={styles.mediaContainer}>
        {/* when stop === false, the gif will be shown. when stop === true, null will be returned */}
        {stop === false ? (
          <Image
            style={{width: 230, height: 230}}
            source={{
              uri: 'https://media.giphy.com/media/Kx82Lvb7wcYNO/giphy.gif',
            }}
          />
        ) : null}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingTop: 35,
    padding: 20,
  },
  mediaContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 300,
    width: 300,
    margin: 50,
    backgroundColor: '#bbb',
    borderRadius: 30,
  },
});

export default GifPage;
