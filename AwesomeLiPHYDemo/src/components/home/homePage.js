import React, {useState, useEffect} from 'react';

import {
  View,
  Text,
  StyleSheet,
  useColorScheme,
  StatusBar,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native'; // import basic react native components

// import the UI of the row
import AppItem from './appItem';

// import constants texts specific to the page
import * as Constants from '../../utils/constants/constants';

/**
 * Home page
 *
 * @param {*} props Putting props here for navigation (so we can use props.navigaiton.push(*destination*)).
 * Can store other properties if wanted
 * @returns
 */
const Home = props => {
  /**
   * row data in the app list with (title, description, navigation tag & image)
   * saved the constants in another file ('../../utils/constants/constants') for better organisation
   */
  const [appList, setAppList] = useState([
    {
      itemName: Constants.introPage.title,
      description: Constants.introPage.description,
      navigation: Constants.navigationTag.intro.tag,
      imageDirectory: require('../../assets/images/liphy_intro.png'),
    },
    {
      itemName: Constants.picPage.title,
      description: Constants.picPage.description,
      navigation: Constants.navigationTag.pic.tag,
      imageDirectory: require('../../assets/images/vlpic.png'),
    },
    {
      itemName: Constants.playPage.title,
      description: Constants.playPage.description,
      navigation: Constants.navigationTag.play.tag,
      imageDirectory: require('../../assets/images/vlgif.png'),
    },
    {
      itemName: Constants.gamePage.title,
      description: Constants.gamePage.description,
      navigation: Constants.navigationTag.game.tag,
      imageDirectory: require('../../assets/images/vlgame1.png'),
    },
    {
      itemName: Constants.cloudPage.title,
      description: Constants.cloudPage.description,
      navigation: Constants.navigationTag.cloud.tag,
      imageDirectory: require('../../assets/images/vlcloud.png'),
    },
    // {
    //   itemName: Constants.navBeginPage.title,
    //   description: Constants.navBeginPage.description,
    //   navigation: Constants.navigationTag.navBegin.tag,
    //   imageDirectory: require('../../assets/images/nav1.jpg'),
    // },
    // {
    //   itemName: Constants.navAdvancePage.title,
    //   description: Constants.navAdvancePage.description,
    //   navigation: Constants.navigationTag.navAdvance.tag,
    //   imageDirectory: require('../../assets/images/nav2.jpg'),
    // },
  ]);

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: '#eeeeee',
      }}>
      <StatusBar barStyle="light-content" />

      {/* title */}
      <Text style={styles.headerText}>LiPHY-IoT Mobile App Courses</Text>

      {/**
       * app list
       * rendering a list where items(AppItem) have similar UI
       */}
      <FlatList
        style={{paddingTop: 25, padding: 14}}
        keyExtractor={(item, index) => index.toString()}
        data={appList}
        // a single element in appList is defined as item here
        renderItem={({item}) => (
          // wrapping appItem with TouchableOpacity so we can have onPress callback
          <TouchableOpacity
            // onPress will be triggered when user press on the item
            onPress={() => {
              // navigating to the tag stated in item.navigation (each item has diffrerent navigation tag)
              props.navigation.push(item.navigation);
            }}>
            {/* UI of the row */}
            <AppItem appDetail={item} />
          </TouchableOpacity>
        )}
        // an empty space for completing the UI
        ListFooterComponent={<View style={{height: 20}} />}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 200,
    paddingTop: 30,
    paddingBottom: 50,
  },
  headerText: {
    backgroundColor: '#fff',
    width: '100%',
    paddingTop: 20,
    paddingBottom: 20,

    // shadow
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.25,
    shadowRadius: 8,

    // font
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center',
  },
});

export default Home;
