import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, Image, useWindowDimensions} from 'react-native'; // import basic react native components

const AppItem = props => {
  const screenWidth = useWindowDimensions().width; // getting the screen width even when using different
  const {itemName, description, imageDirectory} = props.appDetail; // getting the input params from props

  return (
    <View style={styles.appItemrow}>
      {/* image */}
      <Image
        style={{...styles.appPhoto, width: screenWidth - 28}}
        source={imageDirectory}
      />

      {/* title and description */}
      <View style={styles.appText}>
        <Text style={styles.title}>{itemName}</Text>
        <Text>{description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 19,
  },
  appItemrow: {
    marginBottom: 20,
    borderRadius: 20,
    backgroundColor: '#ffffff',

    // shadow
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
  },

  appPhoto: {
    flex: 1,
    height: 200,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  appText: {
    padding: 16,
    paddingBottom: 20,
  },
});

export default AppItem;
