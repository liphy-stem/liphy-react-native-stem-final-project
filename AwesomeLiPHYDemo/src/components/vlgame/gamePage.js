import React, {useState, useEffect, useRef} from 'react';
import {View, Text, Button, StyleSheet, Image, Animated} from 'react-native'; // import basic react native components

import LightManager from 'react-native-liphy'; // import liphy sdk

const GamePage = () => {
  const [coin, setCoin] = useState('head');
  // const [count, setCount] = useState(0);
  const animatedValue = useRef(new Animated.Value(0)).current;
  const [stop, setStop] = useState(true);

  const frontInterpolate = animatedValue.interpolate({
    inputRange: [0, 180],
    outputRange: ['0deg', '180deg'],
  });

  const backInterpolate = animatedValue.interpolate({
    inputRange: [0, 180],
    outputRange: ['180deg', '360deg'],
  });

  const frontAnimatedStyle = {
    transform: [{rotateX: frontInterpolate}],
  };

  const backAnimatedStyle = {
    transform: [{rotateX: backInterpolate}],
  };

  const [lightId, setLightId] = useState();
  const [lightCount, setLightCount] = useState(0);

  const flipDuration = 120;
  const lastingDuration = 1500;

  var flipContinuously;
  var stopAnimation;

  useEffect(() => {
    // setup lightManager
    LightManager.startTracking();
    LightManager.setIsFront(false);

    const listener = LightManager.addEventListener(data => {
      // console.log('light id in introPage: ' + data.lightId);
      setLightId(data.lightId ? data.lightId : 'undefined light ID');
      setLightCount(lightCount + 1);

      var currentdate = new Date();
      var datetime =
        'time: ' +
        currentdate.getHours() +
        ':' +
        currentdate.getMinutes() +
        ':' +
        currentdate.getSeconds() +
        ':' +
        currentdate.getMilliseconds();

      console.log(`light id: ${data.lightId}, ${datetime}`);
    });
    return () => {
      console.log('listener in gamePage');
      listener();
    };
  });
  // }, [lightCount]);

  useEffect(() => {
    // const flipCoin = () => {
    //   Animated.sequence([
    //     Animated.timing(animatedValue, {
    //       toValue: 0,
    //       duration: flipDuration,
    //       useNativeDriver: true,
    //     }),

    //     Animated.timing(animatedValue, {
    //       toValue: 180,
    //       duration: flipDuration,
    //       useNativeDriver: true,
    //     }),

    //     /*
    //   Animated.spring(animatedValue, {
    //     toValue: 0,
    //     tension: 2,
    //     friction: 0.5,
    //     useNativeDriver: true,
    //   }),
    //   Animated.spring(animatedValue, {
    //     toValue: 180,
    //     tension: 2,
    //     friction: 0.5,
    //     useNativeDriver: true,
    //   }),
    //   */
    //   ]).start();

    //   setCoin('head');
    // };

    const fullFlipAnimation = [
      Animated.timing(animatedValue, {
        toValue: 0,
        duration: flipDuration,
        useNativeDriver: true,
      }),

      Animated.timing(animatedValue, {
        toValue: 180,
        duration: flipDuration,
        useNativeDriver: true,
      }),
    ];

    const halfFlipAnimation = [
      Animated.timing(animatedValue, {
        toValue: 0,
        duration: flipDuration,
        useNativeDriver: true,
      }),
    ];

    const flipToHead = () => {
      Animated.sequence([
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
      ]).start();
    };

    const flipToTail = () => {
      Animated.sequence([
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),

        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),

        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),

        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),

        Animated.timing(animatedValue, {
          toValue: 180,
          duration: flipDuration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: 0,
          duration: flipDuration,
          useNativeDriver: true,
        }),
      ]).start();
    };

    const onLightIdChanged = () => {
      var currentdate = new Date();
      var datetime =
        'time: ' +
        currentdate.getHours() +
        ':' +
        currentdate.getMinutes() +
        ':' +
        currentdate.getSeconds() +
        ':' +
        currentdate.getMilliseconds();

      // init or animation is stopped (= can trigger animation again)
      if (flipContinuously === null || stop === true) {
        // random number
        const randomNumber = Math.round(Math.random() * Math.floor(1));
        const resultFlip = randomNumber ? 'tail' : 'head';

        // reset image

        // select animation according to random
        if (resultFlip === 'tail') {
          // animation that flip to tail
          flipToTail();
        } else if (resultFlip === 'head') {
          // animation that flip to head
          flipToHead();
        }

        // // start animation
        // flipContinuously = setInterval(() => {
        //   console.log(`start animation ${datetime}, stop: ${stop}`);
        //   flipCoin();

        //   // final flip

        //   // setStop(true);
        // }, flipDuration * 2 + 1);

        // if (stopAnimation !== null) {
        //   // clear last timer
        //   clearTimeout(stopAnimation);

        //   console.log(`clear stopAnimation timeout ${datetime}`);
        // }

        // // create new timer to stop animation in the future
        // stopAnimation = setTimeout(() => {
        //   clearInterval(flipContinuously);
        //   setStop(true);

        //   console.log(`stopAnimation ${datetime}`);
        // }, lastingDuration);
      } else {
        console.log('flipContinuously !== null || stop !== true');
      }
    };

    return () => {
      console.log('onLightIdChanged');
      onLightIdChanged();
    };
  }, [lightId]);

  // const flipCoin = () => {
  //   const randomNumber = Math.round(Math.random() * Math.floor(1));
  //   const resultFlip = randomNumber ? 'tail' : 'head';

  //   if (resultFlip === 'tail') {
  //     Animated.timing(animatedValue, {
  //       toValue: 0,
  //       duration: 200,
  //     }).start();
  //   } else if (resultFlip === 'head') {
  //     Animated.timing(animatedValue, {
  //       toValue: 180,
  //       duration: 200,
  //     }).start();
  //   }

  //   setCoin(resultFlip);
  // };

  // const continuouslyFlipCoin = () => {
  //   const duration = 120;

  //   setInterval(() => {
  //     Animated.sequence([
  //       Animated.timing(animatedValue, {
  //         toValue: 0,
  //         duration: duration,
  //         useNativeDriver: true,
  //       }),

  //       Animated.timing(animatedValue, {
  //         toValue: 180,
  //         duration: duration,
  //         useNativeDriver: true,
  //       }),

  //       /*
  //       Animated.spring(animatedValue, {
  //         toValue: 0,
  //         tension: 2,
  //         friction: 0.5,
  //         useNativeDriver: true,
  //       }),
  //       Animated.spring(animatedValue, {
  //         toValue: 180,
  //         tension: 2,
  //         friction: 0.5,
  //         useNativeDriver: true,
  //       }),
  //       */
  //     ]).start();
  //   }, duration * 2 + 1);
  // };

  return (
    <View style={styles.container}>
      <View>
        <Animated.Image
          style={[{...styles.coin}, frontAnimatedStyle]}
          source={require('../../assets/images/dogeFront.png')}
        />
        <Animated.Image
          style={[
            {
              ...styles.coin,
              top: 0,
              position: 'absolute',
            },
            backAnimatedStyle,
          ]}
          source={require('../../assets/images/dogeBack.png')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  coin: {
    width: 200,
    height: 200,
    padding: 50,
    margin: 30,
    backfaceVisibility: 'hidden',
  },
});

export default GamePage;
