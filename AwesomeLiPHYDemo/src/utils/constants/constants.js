// navigation constant
export const navigationTag = {
  intro: {
    tag: 'IntroPage',
  },
  pic: {
    tag: 'PicPage',
  },
  play: {
    tag: 'GifPage',
  },
  game: {
    tag: 'GamePage',
  },
  navBegin: {
    tag: 'NavigationPageBeginner',
  },
  cloud: {
    tag: 'CloudPage',
  },
  navAdvance: {
    tag: 'NavigationPageAdvanced',
  },
};

// introPageTag = 'IntroPage';
//   picPageTag = 'PicPage';
//   playPageTag = 'PlayPage';
//   gamePageTag = 'GamePage';
//   navBeginPageTag = 'NavigationPageBeginner';
//   cloudPageTag = 'CloudPage';
//   navAdvancePageTag = 'NavigationPageAdvanced';

export const introPage = {
  title: 'Introduction',
  description: 'Basic operation with LiPHY',
};

export const picPage = {
  title: 'Part 1: VLPic',
  description:
    'Fetch the image using network API & change it according with different LiPHY signals!',
};

export const playPage = {
  title: 'Part 2: VLGif',
  description: "Let's play Gif with LiPHY!!",
};

export const gamePage = {
  title: 'Part 3: Flip the Dogecoin!',
  description: 'Not your normal coins',
  // description: 'We will teach you to play Dice, Roulette, Slots with LiPHY',
};

export const cloudPage = {
  title: 'Part 4: Connecting with LiPHY Cloud',
  description: 'Understand the capability of cloud',
};

export const navBeginPage = {
  title: 'Part 5: Indoor Navigation (Beginner)',
  description: '***** Just a placeholder for now *****',
};

export const navAdvancePage = {
  title: 'Part 6: Indoor Navigation (Advanced)',
  description: '***** Just a placeholder for now *****',
};

// // page title & description
// export const introPageTitle = 'Introduction';
// export const introPageDescription = 'Basic operation with LiPHY';
// // export const introPageHeaderPic = require('/intro.jpg');

// export const picPageTitle = 'Part 1: VLPic';
// export const picPageDescription =
//   'Change the image according with different LiPHY signal!';
// // export const picPageHeaderPic = require('/pic.jpg');

// export const playPageTitle = 'Part 2: VLPlay/VLMedia';
// export const playPageDescription = "Let's play Gif, Video, Song with LiPHY";
// // export const playPageHeaderPic = require('./play.jpg');

// export const gamePageTitle = 'Part 3: VLGame';
// export const gamePageDescription =
//   'We will teach you to play Dice, Roulette, Slots with LiPHY';
// // export const gamePageHeaderPic = require('./game.jpg');

// export const navBeginPageTitle = 'Part 4: Indoor Navigation (Beginner)';
// export const navBeginPageDescription = '***** Just a placeholder for now *****';
// // export const navBeginPageHeaderPic = require('./nav1.jpg');

// export const cloudPageTitle = 'Part 5: Connecting with Cloud';
// export const cloudPageDescription = '***** Just a placeholder for now *****';
// // export const cloudPageHeaderPic = require('./cloud.jpg');

// export const navAdvancePageTitle = 'Part 6: Indoor Navigation (Advanced)';
// export const navAdvancePageDescription =
//   '***** Just a placeholder for now *****';
// // export const navAdvancePageHeaderPic = require('./nav2.jpg');
