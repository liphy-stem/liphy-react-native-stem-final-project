// input lightId, width, height and get a unique url out of it
export const getImageUrl = (lightId, width, height) => {
  return `https://picsum.photos/id/${lightId}/${width}/${height}`;
};
