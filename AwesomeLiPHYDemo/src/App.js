import React, {useState} from 'react';

// components
import MainNavigation from './navigation/mainNavigation';

const App = () => {
  return <MainNavigation />;
};

export default App; // will eventually be used in index.js
